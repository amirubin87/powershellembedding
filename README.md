# README #

This repo holds a notebook to load and build a [Word2vec](https://en.wikipedia.org/wiki/Word2vec) embedding of PowerShell tokens using a corpus of PowerShell content (scripts and modules) from public repositories, available thanks to the [Revoke-Obfuscation](https://github.com/danielbohannon/Revoke-Obfuscation) framework.

This is an implementation based on the work ["AMSI-Based Detection of Malicious PowerShell Code Using Contextual Embeddings"](https://arxiv.org/abs/1905.09538).


### Citation ###

If you use this in your work please cite:

@inproceedings{hendler2020amsi,  
 title={AMSI-Based Detection of Malicious PowerShell Code Using Contextual   Embeddings},  
 author={Hendler, Danny and Kels, Shay and Rubin, Amir},  
 booktitle={Proceedings of the 2020 on Asia Conference on Computer and Communications Security},  
 year={2020}  
}

### How do I get set up? ###

1. To build the embedding on your own - download the corpus zip file from [here](https://aka.ms/PowerShellCorpus).
2. Install required packages - Gensim, numpy, matplotlib


### Who do I talk to? ###

Feedback is welcomed at amirrub[at]cs.bgu.ac.il